<?php

namespace traits;

use app\models\Pizza;
use app\models\Ingredient;

trait hasIngredients
{
    /**
     * @param Pizza $pizza
     * @param array|null $allergens
     * @return bool
     * @throws \Exception
     */
    public function hasAllergens(Pizza $pizza, $allergens)
    {
        if ($pizza && $allergens) {
            foreach ($allergens as $item) {
                $allergen = Ingredient::find()->where(['id' => $item["id"]])->one();
                if ($allergen) {
                    $pizza_allergen = PizzaIngredient::find()->where(['pizza_id' => $pizza->id, 'ingredient_id' => $ingredient->id]);
                    if ($pizza_allergen) return true;
                }

            }
        }
        return false;
    }

    /**
     * @param Pizza $pizza
     * @param array|null $foodTypes
     * @return bool
     * @throws \Exception
     */
    public function hasFoodTypes(Pizza $pizza, $foodTypes)
    {
        if ($pizza && $foodTypes) {
            foreach ($foodTypes as $item) {
                $foodType = Ingredient::find()->where(['id' => $item["id"]])->one();
                if ($foodType) {
                    $pizza_food_type = PizzaIngredient::find()->where(['pizza_id' => $pizza->id, 'ingredient_id' => $ingredient->id]);
                    if ($pizza_food_type) return true;
                }

            }
        }
        return false;
    }

    /**
     * @param Pizza $pizza
     * @param array|null $ingredients
     * @return bool
     * @throws \Exception
     */
    public function removeIngredients(Pizza $pizza, $ingredients)
    {
        if ($pizza && $ingredients) {
            foreach ($foodTypes as $item) {
                $ingredient = Ingredient::find()->where(['id' => $item["id"]])->one();
                if ($ingredient) {
                    $pizza_ingredient = PizzaIngredient::find()->where(['pizza_id' => $pizza->id, 'ingredient_id' => $ingredient->id]);
                    if ($pizza_ingredient)
                    {
                        $pizza_ingredient->delete();
                    }
                }

            }
        }
        return $pizza;
    }

    /**
     * @param Pizza $pizza
     * @param array|null $ingredients
     * @return bool
     * @throws \Exception
     */
    public function doubleIngredients(Pizza $pizza, $ingredients)
    {
        if ($pizza && $ingredients) {
            foreach ($ingredients as $item) {
                $ingredient = Ingredient::find()->where(['id' => $item["id"]])->one();
                if ($ingredient) {
                    $pizza_ingredient = PizzaIngredient::find()->where(['pizza_id' => $pizza->id, 'ingredient_id' => $ingredient->id]);
                    if ($pizza_ingredient)
                    {
                        $pizza_ingredient->pizza_id = $pizza->id;
                        $pizza_ingredient->ingredient_id = $ingredient->id;
                        $pizza_ingredient->quantity = $pizza_ingredient["quantity"]*2;
                        $pizza_ingredient->save();
                    }
                }

            }
        }
        return $pizza;
    }

    /**
     * @param Pizza $pizza
     * @param array|null $ingredients
     * @return bool
     * @throws \Exception
     */
    public function getCalories(Pizza $pizza)
    {
        $total_calories = 0;
        if ($pizza) {
            foreach ($pizza->getPizzaIngredients() as $item) {
                $ingredient = Ingredient::find()->where(['id' => $item["id"]])->one();
                if ($ingredient) {
                    // todo: add Calories into Ingredient model
                    //$total_calories += $ingredient->getCalories();
                }

            }
        }
        return $total_calories;
    }
}
