<?php
/*

Input: a string of unknown length that can contain only three characters: 0, 1, or *. For example: "101*011*1”
Output: an array of strings where each string is a permutation of the input string with the * character replaced by a 0 or a 1.

For example: For input "*1" the result array is ["01", "11"]

We pass index of next character to the recursive function. If the current character is a wildcard character '*',
we replace it with '0 or '1' and do the same for all remaining characters. We print the string if we reach at its end.
Below is recursive the implementation.

Algorithm:

Step 1: Initialize the string first with some wildcard characters in it.

Step 2: Check if index position is equals to the size of string, If it is return.

Step 3: If wildcard character is present at index location, replace it by 0 or 1 accordingly.

Step 4: Print the output

 */

/**
 * @param string $str
 * @param integer $index
 * @return void
 */
function printPermutations($str, $index)
{
    if ($index == strlen($str))
    {
        echo $str."\n";
        return;
    }

    if ($str[$index] == '*')
    {
        // replace '*' by '0' and recurse
        $str[$index] = '0';
        printPermutations($str, $index + 1);

        // replace '*' by '1' and recurse
        $str[$index] = '1';
        printPermutations($str, $index + 1);
    }
    else {
        printPermutations($str, $index + 1);
    }
}

// Driver code
$str = "101*0*1*1";

printPermutations($str, 0);
