
Exercise 1: I stored it at “permutations/index.php” file. I commented the solution at in this file. Run index.php file on Apache and PHP env.

Exercise 2: I stored it at “data_model/api-pizza/” folder. This is just code, not tested.
Currently, I am using the “Yii2” framework to build the REST API Application with MVC modal. It includes:
• I implemented the APIs at The “data_model/api-pizza/controllers/api/” folder.
• I implemented the 3 Models (Pizza, Ingredient and PizzaIngredient) at The “data_model/api-pizza/models/” folder and migrations at “Emurgo/data_model/api-pizza/migrations/” folder.
• I implemented the some functions such as hasAllergens, hasFoodTypes,… in Traits (data_model/api-pizza/traits/hasIngredients.php) to easy using at APIs controllers.
Please read more at data_model/api-pizza/README.md file to understand the Pizza API

Exercise 3: I stored it at “transaction/transaction.py” file, it includes the test example data. Please run this file on Python env.
Ex: python transaction.py or run it at https://ideone.com/p8iL3x link

Exercise 4: I stored it at “image_gallery/index.html” file. Click index.html to run.
