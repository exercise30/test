
def caculate(closingAccounts, recipientAccounts):
	
	transfers = []
	operationalFee = 0;

	n = len(closingAccounts)
	m = len(recipientAccounts)
	p1 = 0
	p2 = 0

	sumB = 0
	for i in range(0, m):
		sumB += recipientAccounts[i]["amount"]

	while True	:

		if p1 >= n or p2 >= m:
			break

		if closingAccounts[p1]["amount"] >= recipientAccounts[p2]["amount"]:

			add = recipientAccounts[p2]["amount"]
			transfers.append([closingAccounts[p1]["accountId"], recipientAccounts[p2]["accountId"], add])
			closingAccounts[p1]["amount"] -= add
			p2 = p2 + 1
		
		elif closingAccounts[p1]["amount"] < recipientAccounts[p2]["amount"]:

			add = closingAccounts[p1]["amount"]
			transfers.append([closingAccounts[p1]["accountId"], recipientAccounts[p2]["accountId"], add])
			p1 = p1 + 1
			recipientAccounts[p2]["amount"] -= add

		operationalFee += 10

	if p1 < n:
		for i in range(p1, n):
			transfers.append([closingAccounts[i]["accountId"], "null", closingAccounts[i]["amount"]])
			operationalFee += 10

	if p2 < m:
		return "not enough funds for rebalance"

	transfers_len = len(transfers)

	for i in range (transfers_len - 1, 0, -1):
		if transfers[i][2] > operationalFee:
			transfers[i][2] -= operationalFee
			break
		else:
			operationalFee -= transfers[i][2]
			transfers = transfers[0:-1]
	
	sum_validate = 0
	for i in range(0, len(transfers)):
		sum_validate += transfers[i][2]

	if sum_validate < sumB:
		return "not enough funds for rebalance"

	return {
		"transfers": transfers,
		"operationalFee": operationalFee
	}


res1 = caculate(
		[
			{
				"accountId": "acc1",
				"amount": 500
			},
			{
				"accountId": "acc2",
				"amount": 500
			}
		],
		[
			{
				"accountId": "rec1",
				"amount": 400
			}
		]

	)
print(res1)

res2 = caculate(
		[
			{
				"accountId": "acc1",
				"amount": 1000
			}
		],
		[
			{
				"accountId": "rec1",
				"amount": 500
			},
			{
				"accountId": "rec2",
				"amount": 400
			}
		]

	)
print(res2)

res3 = caculate(
		[
			{
				"accountId": "acc1",
				"amount": 300
			},
			{
				"accountId": "acc2",
				"amount": 600
			}
		],
		[
			{
				"accountId": "rec1",
				"amount": 500
			},
			{
				"accountId": "rec2",
				"amount": 400
			}
		]

	)
print(res3)